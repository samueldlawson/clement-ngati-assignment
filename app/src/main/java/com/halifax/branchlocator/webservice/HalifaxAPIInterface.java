package com.halifax.branchlocator.webservice;


import com.halifax.branchlocator.model.Branches;
import retrofit2.Call;
import retrofit2.http.GET;


public interface HalifaxAPIInterface {

    String version = "open-banking/v2.2";

    @GET(version + "/branches")
    Call<Branches> getBranches();

}
