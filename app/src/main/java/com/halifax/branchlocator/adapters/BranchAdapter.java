package com.halifax.branchlocator.adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halifax.branchlocator.R;
import com.halifax.branchlocator.intefaces.BranchClickListenerInterface;
import com.halifax.branchlocator.model.Branch;
import com.halifax.branchlocator.model.PostalAddress;

import java.util.ArrayList;

/**
 * Adapter responsible for loading specific branch information in the Recycler View list.
 */
public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.MyViewHolder> {
    private Context mContext;
    private FragmentManager mFragmentManager;
    private BranchClickListenerInterface mBranchClickListenerInterface;
    private ArrayList<Branch> mBranchResults = new ArrayList<>();
    private int lastPosition=-1;

    public BranchAdapter(ArrayList<Branch> branchResults, Context context, FragmentManager fragmentManager, BranchClickListenerInterface branchClickListenerInterface) {
        this.mContext = context;
        this.mFragmentManager = fragmentManager;
        this.mBranchClickListenerInterface = branchClickListenerInterface;
        this.mBranchResults = branchResults;
    }

    @Override
    public BranchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.branch_list_single_item, parent, false);
        return new BranchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.textViewBranchName.setText(mBranchResults.get(position).getName());

        PostalAddress address = mBranchResults.get(position).getPostalAddress();
        String location = address.getTownName();
        if (address.getCountrySubDivision().size() > 0) {
            location = location + ", " + address.getCountrySubDivision().get(0);
        }
        location = location + " " + address.getCountry();

        holder.textViewLocationName.setText(location);
        holder.mBranchContainerLinearLayOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBranchClickListenerInterface.onItemClickCallBack(position);
            }
        });

        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;

    }


    @Override
    public int getItemCount() {
        return mBranchResults.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewBranchName;
        public TextView textViewLocationName;
        private LinearLayout mBranchContainerLinearLayOut;


        public MyViewHolder(View view) {
            super(view);
            textViewBranchName = (TextView) view.findViewById(R.id.branchNameTextView);
            textViewLocationName = (TextView) view.findViewById(R.id.locationTextView);
            mBranchContainerLinearLayOut = (LinearLayout) view.findViewById(R.id.branchContainerLinearLayout);
        }

    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.textViewBranchName.clearAnimation();
    }
}
