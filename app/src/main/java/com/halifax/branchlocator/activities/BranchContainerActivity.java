package com.halifax.branchlocator.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.halifax.branchlocator.R;
import com.halifax.branchlocator.fragments.BranchDetailsFragment;
import com.halifax.branchlocator.fragments.BranchListingFragment;


/**
 * Holds BranchListingFragment and BranchDetailsFragment
 */
public class BranchContainerActivity extends AppCompatActivity implements BranchListingFragment.OnFragmentInteractionListener,
BranchDetailsFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_container);
        intUi();
    }

    private void intUi() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = BranchListingFragment.newInstance();
        if (fragment != null) {
            fragmentManager.beginTransaction().add(R.id.branchcontainer, fragment).commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
