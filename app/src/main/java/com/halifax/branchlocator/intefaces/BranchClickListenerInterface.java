package com.halifax.branchlocator.intefaces;

public interface BranchClickListenerInterface {
    void onItemClickCallBack(int position);
}
