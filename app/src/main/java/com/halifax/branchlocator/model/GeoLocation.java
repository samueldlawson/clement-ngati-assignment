package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoLocation {

    @SerializedName("Geolocation")
    @Expose
    private GeographicCoordinates geographicCoordinates;

    public GeographicCoordinates getGeographicCoordinates() {
        return geographicCoordinates;
    }
}
