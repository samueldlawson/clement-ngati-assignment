package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpeningHour {

    @SerializedName("OpeningTime")
    @Expose
    private String openingTime;


    @SerializedName("ClosingTime")
    @Expose
    private String closingTime;


    public String getOpeningTime() {
        return openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }
}
