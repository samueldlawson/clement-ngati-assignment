package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StandardAvailability {

    @SerializedName("Day")
    @Expose
    private ArrayList<DayOfOperation> availableDays  = new ArrayList<>();

    public ArrayList<DayOfOperation> getAvailableDays() {
        return availableDays;
    }

}
