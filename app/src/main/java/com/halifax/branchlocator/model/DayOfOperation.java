package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DayOfOperation {

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("OpeningHours")
    @Expose
    private ArrayList<OpeningHour> openingHours = new ArrayList<>();

    public String getName() {
        return name;
    }

    public ArrayList<OpeningHour> getOpeningHours() {
        return openingHours;
    }

}
