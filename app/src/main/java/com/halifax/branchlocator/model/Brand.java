package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Brand {

    @SerializedName("BrandName")
    @Expose
    private String brandName;

    @SerializedName("Branch")
    @Expose
    private ArrayList<Branch> branches  = new ArrayList<>();

    public String getBrandName() {
        return brandName;
    }

    public ArrayList<Branch> getBranches() {
        return branches;
    }
}
