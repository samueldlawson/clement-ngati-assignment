package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Availability {

    @SerializedName("StandardAvailability")
    @Expose
    private StandardAvailability standardAvailability;

    public StandardAvailability getStandardAvailability() {
        return standardAvailability;
    }
}
