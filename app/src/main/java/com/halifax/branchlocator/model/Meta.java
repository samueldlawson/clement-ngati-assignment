package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("LastUpdated")
    @Expose
    private String LastUpdated;

    @SerializedName("TotalResults")
    @Expose
    private String totalResults;

    @SerializedName("Agreement")
    @Expose
    private String agreement;

    @SerializedName("License")
    @Expose
    private String license;

    @SerializedName("TermsOfUse")
    @Expose
    private String termsOfUse;


    public String getLastUpdated() {
        return LastUpdated;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public String getAgreement() {
        return agreement;
    }

    public String getLicense() {
        return license;
    }

    public String getTermsOfUse() {
        return termsOfUse;
    }

}
