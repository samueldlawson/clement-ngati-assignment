package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PostalAddress {

    @SerializedName("AddressLine")
    @Expose
    private ArrayList<String> addressLines = new ArrayList<>();

    @SerializedName("TownName")
    @Expose
    private String townName;

    @SerializedName("CountrySubDivision")
    @Expose
    private ArrayList<String> countrySubDivision  = new ArrayList<>();

    @SerializedName("Country")
    @Expose
    private String country;

    @SerializedName("PostCode")
    @Expose
    private String postalCode;

    @SerializedName("GeoLocation")
    @Expose
    private GeoLocation geoLocation;

    public ArrayList<String> getAddressLines() {
        return addressLines;
    }

    public String getTownName() {
        return townName;
    }

    public ArrayList<String> getCountrySubDivision() {
        return countrySubDivision;
    }

    public String getCountry() {
        return country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }
}
