package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Branches {

    @SerializedName("meta")
    @Expose
    private Meta meta;

    @SerializedName("data")
    @Expose
    private ArrayList<Brands> brands = new ArrayList<>();

    public Meta getMeta() {
        return meta;
    }

    public ArrayList<Brands> getBrandsDataList() {
        return brands;
    }
}
