package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("ContactType")
    @Expose
    private String contactType;

    @SerializedName("ContactContent")
    @Expose
    private String contactContent;

    public String getContactType() {
        return contactType;
    }

    public String getContactContent() {
        return contactContent;
    }
}
