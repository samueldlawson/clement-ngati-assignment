package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Branch {

    @SerializedName("Identification")
    @Expose
    private String identification;

    @SerializedName("SequenceNumber")
    @Expose
    private String sequenceNumber;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Type")
    @Expose
    private String type;

    @SerializedName("CustomerSegment")
    @Expose
    private ArrayList<String> customerSegment  = new ArrayList<>();

    @SerializedName("Accessibility")
    @Expose
    private ArrayList<String> accessibility = new ArrayList<>();

    @SerializedName("OtherServiceAndFacility")
    @Expose
    private ArrayList<BranchService> branchServices = new ArrayList<>();

    @SerializedName("PostalAddress")
    @Expose
    private PostalAddress postalAddress;

    @SerializedName("ContactInfo")
    @Expose
    private ArrayList<Contact> contactInfo = new ArrayList<>();

    @SerializedName("Availability")
    @Expose
    private Availability availability;

    public String getIdentification() {
        return identification;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public ArrayList<BranchService> getBranchServices() {
        return branchServices;
    }

    public ArrayList<String> getCustomerSegment() {
        return customerSegment;
    }

    public ArrayList<String> getAccessibility() {
        return accessibility;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public ArrayList<Contact> getContactInfo() {
        return contactInfo;
    }

    public Availability getAvailability() {
        return availability;
    }
}

