package com.halifax.branchlocator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Brands {

    @SerializedName("Brand")
    @Expose
    private ArrayList<Brand> brands  = new ArrayList<>();;

    public ArrayList<Brand> getBrands() {
        return brands;
    }
}

