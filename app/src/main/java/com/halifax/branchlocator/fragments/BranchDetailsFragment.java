package com.halifax.branchlocator.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.halifax.branchlocator.R;
import com.halifax.branchlocator.model.Branch;
import com.halifax.branchlocator.model.Contact;
import com.halifax.branchlocator.model.DayOfOperation;
import com.halifax.branchlocator.model.OpeningHour;
import com.halifax.branchlocator.util.PreferencesHandler;
import com.halifax.branchlocator.webservice.HalifaxAPIInterface;

/**
 * Displays specific Branch's detailed information
 */
public class BranchDetailsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private static Branch mBranch;
    private View mRootView;
    private HalifaxAPIInterface mHalifaxApiInterface;
    private Typeface mTypeface;
    private ProgressBar mBranchDetailsProgressBar;
    private FragmentManager fragmentManager;
    private Fragment mFragment;
    private PreferencesHandler mPreferencesHandler;
    private ImageView mBackArrow;
    private RelativeLayout mParentContanerLayout;
    private TextView mBranchTextView;
    private TextView mAddressTextView;
    private TextView mPhoneTextView;
    private TextView mHoursTextView;

    public BranchDetailsFragment() { }

    public static BranchDetailsFragment newInstance(Branch branch) {
        BranchDetailsFragment fragment = new BranchDetailsFragment();
        mBranch = branch;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_branch_details, container, false);
        initUi();
        return mRootView;
    }

    private void initUi() {
        mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Cabin-Regular.ttf");
        mBranchDetailsProgressBar = (ProgressBar) mRootView.findViewById(R.id.branchDetailsProgressBar);
        mBackArrow=(ImageView)mRootView.findViewById(R.id.backArrow);
        mParentContanerLayout=(RelativeLayout)mRootView.findViewById(R.id.mParentContainerLayout);
        mPreferencesHandler=PreferencesHandler.getInstance(getActivity());
        mParentContanerLayout.setVisibility(View.GONE);
        mBranchTextView = (TextView) mRootView.findViewById(R.id.branchTextView);
        mAddressTextView = (TextView) mRootView.findViewById(R.id.addressTextView);
        mPhoneTextView = (TextView) mRootView.findViewById(R.id.phoneTextView);
        mHoursTextView = (TextView) mRootView.findViewById(R.id.hoursTextView);
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        setValuesToView();
    }

    /**
     * load branch information details
     */
    private void setValuesToView() {

        mParentContanerLayout.setVisibility(View.VISIBLE);
        mBranchDetailsProgressBar.setVisibility(View.GONE);

        mBranchTextView.setText(mBranch.getName());

        String address = "--";
        if (mBranch.getPostalAddress() != null) {
            address = "";
            for (String line: mBranch.getPostalAddress().getAddressLines()) {
                address = address + line + "\n\r";
            }
            address = address + mBranch.getPostalAddress().getTownName();
            for (String sub: mBranch.getPostalAddress().getCountrySubDivision()) {
                address = address + ", " + sub;
            }
            address = address + ", " + mBranch.getPostalAddress().getPostalCode() + " " + mBranch.getPostalAddress().getCountry();
            mAddressTextView.setText(address);
        }

        String phone = "--";
        if (mBranch.getContactInfo().size() > 0) {
            phone = "";
            for(Contact c: mBranch.getContactInfo()) {
                if (c.getContactType().toLowerCase().trim().equals("phone")) {
                    phone = c.getContactContent();
                    break;
                }
            }
        }
        mPhoneTextView.setText(phone);

        String hours = "--";
        if (mBranch.getAvailability() != null
                && mBranch.getAvailability().getStandardAvailability() != null
                && mBranch.getAvailability().getStandardAvailability().getAvailableDays().size() > 0) {

            hours = "";
            for (DayOfOperation dayOp: mBranch.getAvailability().getStandardAvailability().getAvailableDays()) {
                hours = hours + dayOp.getName() + "\n\r";
                hours = hours + "--------------------" + "\n\r";
                for (OpeningHour oHourday: dayOp.getOpeningHours()) {
                    hours  = hours + oHourday.getOpeningTime() + " - " + oHourday.getOpeningTime() + "\n\r";
                }
                hours = hours + "\n\r";
            }

        }
        mHoursTextView.setText(hours);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
