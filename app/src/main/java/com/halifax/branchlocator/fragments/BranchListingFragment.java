package com.halifax.branchlocator.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.halifax.branchlocator.R;
import com.halifax.branchlocator.adapters.BranchAdapter;
import com.halifax.branchlocator.application.AppController;
import com.halifax.branchlocator.customviews.CustomEditText;
import com.halifax.branchlocator.intefaces.BranchClickListenerInterface;
import com.halifax.branchlocator.model.Branch;
import com.halifax.branchlocator.model.Branches;
import com.halifax.branchlocator.util.PreferencesHandler;
import com.halifax.branchlocator.util.Utils;
import com.halifax.branchlocator.webservice.HalifaxAPIClient;
import com.halifax.branchlocator.webservice.HalifaxAPIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Lists branches based on filtered criteria
 */
public class BranchListingFragment extends Fragment implements BranchClickListenerInterface {

    private OnFragmentInteractionListener mListener;
    private View mRootView;
    private RecyclerView mRecyclerView;
    private BranchAdapter mBranchAdapter;
    private HalifaxAPIInterface mHalifaxApiInterface;
    private PreferencesHandler mPreferencesHandler;
    private Branches mBranches;
    //all results
    private ArrayList<Branch> mBranchAllResults = new ArrayList<>();
    //filtered results
    private ArrayList<Branch> mBranchResults = new ArrayList<>();
    private Dialog dialog;
    private ImageView closeImageView;
    private Button filterByCityButton;
    private Button filterByAllLocationsButton;
    private ProgressBar mProgressBar;
    private boolean isScrollEnabled = true;
    private CustomEditText mCityEditText;
    private TextView mNoBranchesAvailableTextView;
    private ImageView mEditImageView;
    private Button mApplyButton;
    private TextView mTitleHeadingTextView;
    private TextView mLocationTextView;
    private LinearLayout mLayoutCityOptions;
    private Typeface typeFace;
    private Window w;

    public BranchListingFragment() { }

    public static BranchListingFragment newInstance() {
        BranchListingFragment fragment = new BranchListingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_branch_listing, container, false);
        initUi();
        return mRootView;
    }

    private void initUi() {

        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.jobListRecyclerView);
        mPreferencesHandler = PreferencesHandler.getInstance(getActivity());
        mNoBranchesAvailableTextView = (TextView) mRootView.findViewById(R.id.noBranchesTextView);
        mTitleHeadingTextView = (TextView) mRootView.findViewById(R.id.titleHeading);
        mLocationTextView = (TextView) mRootView.findViewById(R.id.locationTextView);
        mLayoutCityOptions = (LinearLayout) mRootView.findViewById(R.id.layoutCityOptions);
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.mJobListProgressBar);
        mEditImageView = (ImageView) mRootView.findViewById(R.id.editImageView);
        typeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/Cabin-Regular.ttf");
        mEditImageView.setOnClickListener(onClickListener);
        mLocationTextView.setOnClickListener(onClickListener);

        setDataToAdapter();
        if (Utils.isNetworkAvailable(getActivity())) {
            mProgressBar.setVisibility(VISIBLE);
            fetchBranchData();
        } else {
            Utils.Toast(getActivity(), AppController.NO_NETWORK_CONNECTION_MESSAGE);
            mNoBranchesAvailableTextView.setVisibility(VISIBLE);
            mNoBranchesAvailableTextView.setText(AppController.NO_NETWORK_CONNECTION_MESSAGE);
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.editImageView:
                    showDialogEdit();
                    break;
                case R.id.locationTextView:
                    showDialogEdit();
                    break;
                default:
                    break;
            }

        }
    };

    private void fetchSmoothly() {
        w = getActivity().getWindow();
        w.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void setDataToAdapter() {
        mBranchAdapter = new BranchAdapter(mBranchResults, getActivity(), getFragmentManager(), this);
        RecyclerView.LayoutManager mLayoutManager = new CustomGridLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mBranchAdapter);
    }

    /**
     * Updates view with filtered or all branch locations depending on selected filter.
     * -Sorts the list based on branch name
     */
    private void updateResults() {
        mBranchResults.clear();
        if (mPreferencesHandler.isFilteringByCity()) {
            for (Branch branch: mBranchAllResults) {
                if (branch.getPostalAddress().getTownName() != null &&
                        branch.getPostalAddress().getTownName().toLowerCase().trim()
                        .contains(mPreferencesHandler.getFilterLocation().toLowerCase().trim())) {
                    mBranchResults.add(branch);
                }
            }
        } else {
            for (Branch branch: mBranchAllResults) {
                mBranchResults.add(branch);
            }
        }

        // sort by Branch Name
        Comparator<Branch> c = (s1, s2) -> s1.getName().compareTo(s2.getName());
        mBranchResults.sort(c);

        mLocationTextView.setText(mPreferencesHandler.getFilterLocation() + " (" + mBranchResults.size() + " found)");
        setDataToAdapter();
    }

    /**
     * Fetches all branches from initially and if updates Results. When initially populated, it just updates the results based on selected filter.
     */
    private void fetchBranchData() {

        if (mBranchAllResults.size() > 0) {
            mProgressBar.setVisibility(VISIBLE);
            updateResults();
            mProgressBar.setVisibility(GONE);
            return;
        }

        fetchSmoothly();
        mHalifaxApiInterface = HalifaxAPIClient.getBranchData().create(HalifaxAPIInterface.class);
        Call call;
        call = mHalifaxApiInterface.getBranches();

        call.enqueue(new Callback() {

            @Override
            public void onResponse(Call call, Response response) {
                w.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                isScrollEnabled = true;
                mTitleHeadingTextView.setVisibility(VISIBLE);
                mEditImageView.setVisibility(VISIBLE);
                mLocationTextView.setVisibility(VISIBLE);
                mLocationTextView.setText(mPreferencesHandler.getFilterLocation());
                if (response.body() != null) {
                    mBranches = (Branches) response.body();
                    if (mBranches != null && mBranches.getBrandsDataList().size() > 0
                            && mBranches.getBrandsDataList().get(0).getBrands().size() > 0) {
                       mBranchAllResults = mBranches.getBrandsDataList().get(0).getBrands().get(0).getBranches();
                       updateResults();
                       mProgressBar.setVisibility(GONE);
                    } else {
                        mProgressBar.setVisibility(GONE);
                    }

                } else {
                    mProgressBar.setVisibility(GONE);
                    if (response.errorBody() != null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("msg"), Toast.LENGTH_LONG).show();
                            String msg = jObjError.getString("msg");
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                w.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mProgressBar.setVisibility(GONE);
                isScrollEnabled = true;
                call.cancel();
            }
        });

    }

    public class CustomGridLayoutManager extends LinearLayoutManager {
        public CustomGridLayoutManager(Context context) {
            super(context);
        }

        @Override
        public boolean canScrollVertically() {
            return isScrollEnabled && super.canScrollVertically();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClickCallBack(int position) {
        FragmentManager fragmentManager = getFragmentManager();
        if (mBranchResults.size() > 0) {
            Fragment fragment = BranchDetailsFragment.newInstance(mBranchResults.get(position));
            if (fragment != null) {
                fragmentManager.beginTransaction().add(R.id.branchcontainer, fragment).addToBackStack(null).commit();
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Launches dialog for allowing user to change filtering options
     */
    private void showDialogEdit() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.filter_pop_up);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        closeImageView = (ImageView) dialog.findViewById(R.id.popupCloseView);
        mCityEditText = (CustomEditText) dialog.findViewById(R.id.cityEditText);
        mApplyButton = (Button) dialog.findViewById(R.id.applyEditButton);
        mLayoutCityOptions = (LinearLayout) dialog.findViewById(R.id.layoutCityOptions);
        filterByCityButton = (Button) dialog.findViewById(R.id.filterByCityButton);
        filterByAllLocationsButton = (Button) dialog.findViewById(R.id.filterByAllLocationsButton);

        mApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mLayoutCityOptions.getVisibility() == VISIBLE) {
                    if (mCityEditText.length() > 0) {
                        mPreferencesHandler.setFilterLocation(mCityEditText.getText().toString().trim());
                        mPreferencesHandler.setFilterOptionToCity();
                        mLocationTextView.setText(mPreferencesHandler.getFilterLocation());
                        dialog.dismiss();
                        mProgressBar.setVisibility(VISIBLE);
                        fetchBranchData();
                    } else {
                        Toast.makeText(getActivity(),"Please enter a City", Toast.LENGTH_LONG).show();
                    }
                } else {
                    mPreferencesHandler.setFilterLocation("All Locations");
                    mPreferencesHandler.setFilterOptionToAllLocations();
                    mLocationTextView.setText(mPreferencesHandler.getFilterLocation());
                    dialog.dismiss();
                    mProgressBar.setVisibility(VISIBLE);
                    fetchBranchData();
                }
            }
        });

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        filterByCityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterByCityEnabled();
            }
        });

        filterByAllLocationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterByAllLocationsEnabled();
            }
        });


        if (mPreferencesHandler.isFilteringByCity()) {
           filterByCityEnabled();
           if (mPreferencesHandler.getFilterLocation().length() > 0
                && !mPreferencesHandler.getFilterLocation().equals("All Locations")) {
               mCityEditText.setText(mPreferencesHandler.getFilterLocation());
           }
        } else {
            filterByAllLocationsEnabled();
        }

        dialog.show();

    }

    /**
     * enables fitler by all locations to enabled and disables filter by city
     */
    private void filterByAllLocationsEnabled() {
        filterByAllLocationsButton.setBackground(getResources().getDrawable(R.drawable.sorting_selected_button));
        filterByAllLocationsButton.setTextColor(getResources().getColor(R.color.textColor));
        filterByCityButton.setTextColor(getResources().getColor(R.color.textColorButton));
        filterByCityButton.setBackgroundColor(getResources().getColor(R.color.transparent));
        mLayoutCityOptions.setVisibility(GONE);
    }

    /**
     * enables filter by city and disables filter by all locations
     */
    private void filterByCityEnabled() {
        filterByCityButton.setBackground(getResources().getDrawable(R.drawable.sorting_selected_button));
        filterByCityButton.setTextColor(getResources().getColor(R.color.textColor));
        filterByAllLocationsButton.setTextColor(getResources().getColor(R.color.textColorButton));
        filterByAllLocationsButton.setBackgroundColor(getResources().getColor(R.color.transparent));
        mLayoutCityOptions.setVisibility(VISIBLE);
    }

}
