package com.halifax.branchlocator.activities;

import android.view.View;

import androidx.test.rule.ActivityTestRule;

import com.halifax.branchlocator.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class BranchContainerActivityTest {

    @Rule
    public ActivityTestRule<BranchContainerActivity> mActivityTestRule = new ActivityTestRule<BranchContainerActivity>(BranchContainerActivity.class);
    private BranchContainerActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testApplicationLaunches() {
        View view = mActivity.findViewById(R.id.branchcontainer);
        assertNotNull(view);
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void onCreate() {
    }

    @Test
    public void onFragmentInteraction() {
    }
}