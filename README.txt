
Folder contains 4 Files:
------------------------------

1. README.txt -- file describing folder contents.

2. Halifax-BranchLocator-SourceCode.zip -- archived android source code.

3. Halifax-BranchLocator-Documentation.docx -- describes app, screenshots and build & deployment process.

4. Halifax-BranchLocator.apk -- prebuilt APK of the app.


- Clement Ngati
- 949.353.4480

